package com.android.okalman.kiwi_app.fragments;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.okalman.kiwi_app.R;
import com.android.okalman.kiwi_app.db.objects.Flight;

import java.text.SimpleDateFormat;

/**
 * Created by okalman on 3.10.17.
 * Fragment which displays flight on activity
 */

public class FlightItemFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.pager_item_fragment_layout, container, false);
        Bundle args = getArguments();
        Flight flight = (Flight) args.getSerializable("flight");

        ((TextView) rootView.findViewById(R.id.textTo)).setText(flight.getToCity() + " (" + flight.getToCountry() + ")");
        ((TextView) rootView.findViewById(R.id.textPrice)).setText(String.valueOf((int) flight.getPrice()) + " Eur");

        String departure = flight.getFromCity() + " - " + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(flight.getDepartureTime());
        String arrival = flight.getToCity() + " - " + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(flight.getArrivalTime());
        ((TextView) rootView.findViewById(R.id.textDeparture)).setText(departure);
        ((TextView) rootView.findViewById(R.id.textArrival)).setText(arrival);
        ((TextView) rootView.findViewById(R.id.textDuration)).setText(flight.getDuration());
        ((TextView) rootView.findViewById(R.id.textDistance)).setText(String.valueOf(flight.getDistance())+" Km");
        ((TextView) rootView.findViewById(R.id.textStopovers)).setText(flight.getStopOvers());

        String toId = flight.getToMapId() + "_" + flight.getToCountryCode().toLowerCase();
        try {
            ((ImageView) rootView.findViewById(R.id.imageTo)).setImageBitmap(BitmapFactory.decodeStream(getContext().openFileInput(toId + ".jpg")));
        } catch (Exception e) {
            Log.w("Pager Fragmet", "Failed to load image for id" + toId, e);
        }


        return rootView;
    }


}
