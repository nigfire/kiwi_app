package com.android.okalman.kiwi_app.db.objects;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;

/**
 * Created by okalman on 1.10.17.
 * Object holding flight data and information for database storage
 */

public class Flight implements Serializable {

    public static final String TABLE_NAME = "flights";
    public static final String COLUMN_ID = "COLUMN_ID";
    public static final String COLUMN_FROM_CITY = "COLUMN_FROM_CITY";
    public static final String COLUMN_FROM_COUNTRY = "COLUMN_FROM_COUNTRY";
    public static final String COLUMN_FROM_COUNTRY_CODE = "COLUMN_FROM_COUNTRY_CODE";
    public static final String COLUMN_TO_CITY = "COLUMN_TO_CITY";
    public static final String COLUMN_TO_COUNTRY = "COLUMN_TO_COUNTRY";
    public static final String COLUMN_TO_COUNTRY_CODE = "COLUMN_TO_COUNTRY_CODE";
    public static final String COLUMN_DEPARTURE = "COLUMN_DEPARTURE";
    public static final String COLUMN_ARRIVAL = "COLUMN_ARRIVAL";
    public static final String COLUMN_PRICE = "COLUMN_PRICE";
    public static final String COLUMN_FROM_MAP_ID = "COLUMN_FROM_MAP_ID";
    public static final String COLUMN_TO_MAP_ID = "COLUMN_TO_MAP_ID";
    public static final String COLUMN_DAY_TO_SHOW = "COLUMN_DAY_TO_SHOW";
    public static final String COLUMN_INVALIDATED = "COLUMN_INVALIDATED";
    public static final String COLUMN_STOPOVERS = "COLUMN_STOPOVERS";
    public static final String COLUMN_DURATION = "COLUMN_DURATION";
    public static final String COLUMN_DISTANCE = "COLUMN_DISTANCE";

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            COLUMN_ID + " STRING PRIMARY KEY," +
            COLUMN_FROM_CITY + " TEXT," +
            COLUMN_FROM_COUNTRY + " TEXT," +
            COLUMN_FROM_COUNTRY_CODE + " TEXT," +
            COLUMN_TO_CITY + " TEXT," +
            COLUMN_TO_COUNTRY + " TEXT," +
            COLUMN_TO_COUNTRY_CODE + " TEXT," +
            COLUMN_DEPARTURE + " INTEGER," +
            COLUMN_ARRIVAL + " INTEGER," +
            COLUMN_PRICE + " REAL," +
            COLUMN_DAY_TO_SHOW + " INTEGER," +
            COLUMN_INVALIDATED + " INTEGER," +
            COLUMN_FROM_MAP_ID + " TEXT," +
            COLUMN_STOPOVERS + " TEXT," +
            COLUMN_DURATION + " TEXT," +
            COLUMN_DISTANCE + " FLOAT," +
            COLUMN_TO_MAP_ID + " TEXT " +
            ")";
    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    private String id;
    private String fromCity;
    private String fromCountry;
    private String fromCountryCode;
    private String toCity;
    private String toCountry;
    private String toCountryCode;
    private String fromMapId;
    private String toMapId;
    private String stopOvers;
    private String duration;
    private double distance;
    private double price;
    private long departureTime, arrivalTime, dayToShow;
    private boolean invalidated;

    public Flight() {

    }

    public Flight(Cursor cursor) {
        id = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ID));
        fromCity = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FROM_CITY));
        fromCountry = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FROM_COUNTRY));
        fromCountryCode = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FROM_COUNTRY_CODE));
        toCity = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TO_CITY));
        toCountry = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TO_COUNTRY));
        toCountryCode = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TO_COUNTRY_CODE));
        fromMapId = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FROM_MAP_ID));
        toMapId = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TO_MAP_ID));
        price = cursor.getFloat(cursor.getColumnIndexOrThrow(COLUMN_PRICE));
        departureTime = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_DEPARTURE));
        arrivalTime = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ARRIVAL));
        dayToShow = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_DAY_TO_SHOW));
        stopOvers = cursor.getString(cursor.getColumnIndex(COLUMN_STOPOVERS));
        duration = cursor.getString(cursor.getColumnIndex(COLUMN_DURATION));
        distance = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_DISTANCE));
        int invalidatedInt = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_INVALIDATED));
        invalidated = invalidatedInt > 0 ? true : false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getFromCountry() {
        return fromCountry;
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getToCountry() {
        return toCountry;
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(long departureTime) {
        this.departureTime = departureTime;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public long getDayToShow() {
        return dayToShow;
    }

    public void setDayToShow(long dayToShow) {
        this.dayToShow = dayToShow;
    }

    public boolean isInvalidated() {
        return invalidated;
    }

    public void setInvalidated(boolean invalidated) {
        this.invalidated = invalidated;
    }

    public String getFromMapId() {
        return fromMapId;
    }

    public void setFromMapId(String fromMapId) {
        this.fromMapId = fromMapId;
    }

    public String getToMapId() {
        return toMapId;
    }

    public void setToMapId(String toMapId) {
        this.toMapId = toMapId;
    }

    public String getFromCountryCode() {
        return fromCountryCode;
    }

    public void setFromCountryCode(String fromCountryCode) {
        this.fromCountryCode = fromCountryCode;
    }

    public String getToCountryCode() {
        return toCountryCode;
    }

    public void setToCountryCode(String toCountryCode) {
        this.toCountryCode = toCountryCode;
    }

    public String getStopOvers() {
        return stopOvers;
    }

    public void setStopOvers(String stopOvers) {
        this.stopOvers = stopOvers;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getIdForPictureFrom() {
        if (fromMapId == null || fromCountryCode == null) {
            return null;
        } else {
            return fromMapId + "_" + fromCountryCode.toLowerCase();
        }
    }

    public String getIdForPictureTo() {
        if (toMapId == null || toCountryCode == null) {
            return null;
        } else {
            return toMapId + "_" + toCountryCode.toLowerCase();
        }
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(Flight.COLUMN_ARRIVAL, this.getArrivalTime());
        values.put(Flight.COLUMN_DAY_TO_SHOW, this.getDayToShow());
        values.put(Flight.COLUMN_DEPARTURE, this.getDepartureTime());
        values.put(Flight.COLUMN_FROM_CITY, this.getFromCity());
        values.put(Flight.COLUMN_FROM_COUNTRY, this.getFromCountry());
        values.put(Flight.COLUMN_FROM_COUNTRY_CODE, this.getFromCountryCode());
        values.put(Flight.COLUMN_FROM_MAP_ID, this.getFromMapId());
        values.put(Flight.COLUMN_ID, this.getId());
        values.put(Flight.COLUMN_INVALIDATED, this.isInvalidated());
        values.put(Flight.COLUMN_PRICE, this.getPrice());
        values.put(Flight.COLUMN_TO_CITY, this.getToCity());
        values.put(Flight.COLUMN_TO_COUNTRY, this.getToCountry());
        values.put(Flight.COLUMN_TO_COUNTRY_CODE, this.getToCountryCode());
        values.put(Flight.COLUMN_STOPOVERS, this.getStopOvers());
        values.put(Flight.COLUMN_DISTANCE, this.getDistance());
        values.put(Flight.COLUMN_DURATION, this.getDuration());
        values.put(Flight.COLUMN_TO_MAP_ID, this.getToMapId());
        return values;
    }
}
