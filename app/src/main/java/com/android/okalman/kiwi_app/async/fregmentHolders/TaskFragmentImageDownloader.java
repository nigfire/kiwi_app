package com.android.okalman.kiwi_app.async.fregmentHolders;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.android.okalman.kiwi_app.async.tasks.AsyncImageDownloader;
import com.android.okalman.kiwi_app.calbacks.ICallback;
import com.android.okalman.kiwi_app.db.objects.Flight;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 3.10.17.
 * This fragment is used to host asynctask and report its result to activity
 */
public class TaskFragmentImageDownloader extends Fragment {


    private ICallback activityCallBack;
    private boolean taskFinished = false;
    private boolean resumed = false;
    private List<Flight> flights;

    /**
     * @param flights
     */
    public static final TaskFragmentImageDownloader getInstance(List<Flight> flights) {
        TaskFragmentImageDownloader fragment = new TaskFragmentImageDownloader();
        Bundle args = new Bundle();
        ArrayList<Flight> arrayFlights = new ArrayList<>(flights);
        args.putSerializable("flights", arrayFlights);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * every time when screen rotates new activity is passed here
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activityCallBack = (ICallback) context;
        if (taskFinished) {
            resumed = true;
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (resumed) {
            onTaskFinished();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true); //keep this instance when screen rotation happens
        this.flights = (ArrayList<Flight>) getArguments().getSerializable("flights");
        AsyncImageDownloader asyncTask = new AsyncImageDownloader(getContext(), new ICallback() {
            @Override
            public void callbackCall(Class callerInstance) {
                TaskFragmentImageDownloader.this.taskFinished = true;
                onTaskFinished();
            }
        });
        asyncTask.execute(flights.toArray(new Flight[flights.size()]));

    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallBack = null;
    }


    /*
    notify activity when fragment is attached to new activity or just finished task for current one
     */
    public void onTaskFinished() {
        activityCallBack.callbackCall(this.getClass());
    }
}
