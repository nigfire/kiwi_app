package com.android.okalman.kiwi_app.json;

import android.util.Log;

import com.android.okalman.kiwi_app.db.objects.Flight;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 1.10.17.
 * Parsing data from API, not very cool to do this this way...
 */

public class FlightHttpJsonParser {

    public static List<Flight> parseFlights(String httpResponse) {
        ArrayList<Flight> flights = new ArrayList<>();
        try {
            JSONObject mainObject = new JSONObject(httpResponse);
            JSONArray data = mainObject.getJSONArray("data");
            for (int i = 0; i < data.length(); i++) {
                Flight flight = new Flight();
                JSONObject jsonFlight = data.getJSONObject(i);
                flight.setId(jsonFlight.getString("id"));
                flight.setFromMapId(jsonFlight.getString("mapIdfrom"));
                flight.setFromCity(jsonFlight.getString("cityFrom"));
                flight.setFromCountry(jsonFlight.getJSONObject("countryFrom").getString("name"));
                flight.setFromCountryCode(jsonFlight.getJSONObject("countryFrom").getString("code"));
                flight.setDepartureTime(jsonFlight.getLong("dTime") * 1000);
                flight.setToMapId(jsonFlight.getString("mapIdto"));
                flight.setToCity(jsonFlight.getString("cityTo"));
                flight.setToCountry(jsonFlight.getJSONObject("countryTo").getString("name"));
                flight.setToCountryCode(jsonFlight.getJSONObject("countryTo").getString("code"));
                flight.setArrivalTime(jsonFlight.getLong("aTime") * 1000);
                flight.setPrice(jsonFlight.getDouble("price"));
                flight.setDuration(jsonFlight.getString("fly_duration"));
                flight.setStopOvers(parseStopOvers(jsonFlight.getJSONArray("route")));
                flight.setDistance(jsonFlight.getDouble("distance"));
                flights.add(flight);
            }

        } catch (Exception e) {
            Log.w("FLIGHT PARSER","parsing object failed",e);
            return null;
        }
        return flights;
    }

    private static String parseStopOvers(JSONArray array) {
        StringBuilder stopOvers = new StringBuilder("");
        try {
            for (int i = 1; i < array.length(); i++) {
                stopOvers.append(array.getJSONObject(i).getString("cityFrom"));
                if (i < array.length() - 1) {
                    stopOvers.append(",");
                }
            }
        } catch (Exception e) {
            Log.w("PARSE STOPOVERS", "failed", e);
        }
        return stopOvers.toString();
    }
}
