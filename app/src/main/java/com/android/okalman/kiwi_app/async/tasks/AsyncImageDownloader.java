package com.android.okalman.kiwi_app.async.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.okalman.kiwi_app.R;
import com.android.okalman.kiwi_app.Utils;
import com.android.okalman.kiwi_app.calbacks.ICallback;
import com.android.okalman.kiwi_app.db.objects.Flight;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by okalman on 2.10.17.
 * This asynctask downloads images of flight destinations, if image is already present download
 * is skipped .
 */

public class AsyncImageDownloader extends AsyncTask<Flight, Void, Void> {

    private Context context;
    private ICallback callback;

    public AsyncImageDownloader(Context context, ICallback callback) {
        this.context = context;
        this.callback = callback;
    }


    @Override
    protected Void doInBackground(Flight... flights) {


        for (Flight f : flights) {
            try {
                String idTo = f.getToMapId() + "_" + f.getToCountryCode().toLowerCase();
                String fileName = idTo + ".jpg";
                if (!Utils.fileExist(fileName, context)) {
                    byte[] image = downloadImage(idTo);
                    FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
                    fos.write(image);
                    fos.close();
                } else {
                    Log.d("IMAGE DOWNLOADER", "Downloading skipped file already exists: " + fileName);
                }
            } catch (Exception e) {
                Log.w("IMAGE DOWNLOADER", "Downloading of image failed due to exception", e);
            }


        }
        return null;
    }

    @Override
    protected void onPostExecute(Void param) {
        if (callback != null) {
            callback.callbackCall(this.getClass());
        }
    }


    private byte[] downloadImage(String id) throws Exception {
        String url = context.getString(R.string.server_url_kiwi_images);
        URL obj = new URL(url + id + ".jpg");
        Log.d("DOWNLOADING", url + id + ".jpg");
        InputStream in = new BufferedInputStream(obj.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while (-1 != (n = in.read(buf))) {
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        Log.d("DOWNLOADING", "Downloaded:" + out.size() + " bytes");
        return out.toByteArray();
    }
}
