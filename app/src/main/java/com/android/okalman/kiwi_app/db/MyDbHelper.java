package com.android.okalman.kiwi_app.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.okalman.kiwi_app.db.objects.Flight;

/**
 * Created by okalman on 1.10.17.
 * This provides database connection
 */

public class MyDbHelper extends SQLiteOpenHelper {


    private static MyDbHelper instance;

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "flightsDB.db";

    private MyDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static MyDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new MyDbHelper(context.getApplicationContext());
        }
        return instance;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("DB_HELPER", "onCreate called");
        sqLiteDatabase.execSQL(Flight.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d("DB_HELPER", "onUpgrade called");
        sqLiteDatabase.execSQL(Flight.DROP_TABLE);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.d("DB_HELPER", "onUpgrade called");
        onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

}
