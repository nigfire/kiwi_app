package com.android.okalman.kiwi_app.async.fregmentHolders;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.android.okalman.kiwi_app.async.tasks.AsyncDataGetter;
import com.android.okalman.kiwi_app.calbacks.IObjectCallback;
import com.android.okalman.kiwi_app.db.objects.Flight;

import java.util.List;

/**
 * Created by okalman on 3.10.17.
 *
 * This fragment is used to host asynctask and report its result to activity
 */

public class TaskFragmentDataGetter extends Fragment {


    private IObjectCallback activityCallBack;
    private boolean taskFinished = false;
    private boolean resumed = false;
    private List<Flight> flights;

    /*
        every time when screen rotates new activity is passed here
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activityCallBack = (IObjectCallback) context;
        if (taskFinished) {
            resumed = true;
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (resumed) {
            onTaskFinished();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true); //keep this instance when screen rotation happens
        runTask();


    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallBack = null;
    }

    public void forceTaskRerun() {
        runTask();
    }


    /*
    notify activity when fragment is attached to new activity or just finished task for current one
     */
    public void onTaskFinished() {
        activityCallBack.callbackCall(this.getClass(), flights);
    }

    private void runTask() {
        AsyncDataGetter asyncTask = new AsyncDataGetter(getContext(), new IObjectCallback() {
            @Override
            public void callbackCall(Class caller, Object data) {
                TaskFragmentDataGetter.this.flights = (List<Flight>) data;
                TaskFragmentDataGetter.this.taskFinished = true;
                onTaskFinished();
            }
        });
        asyncTask.execute();
    }
}
