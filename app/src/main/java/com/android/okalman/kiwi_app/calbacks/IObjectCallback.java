package com.android.okalman.kiwi_app.calbacks;

/**
 * Created by okalman on 3.10.17.
 * Just interface to pass callback
 */

public interface IObjectCallback {
    void callbackCall(Class caller, Object data);
}
