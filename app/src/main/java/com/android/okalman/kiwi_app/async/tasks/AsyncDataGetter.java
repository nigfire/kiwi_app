package com.android.okalman.kiwi_app.async.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.okalman.kiwi_app.R;
import com.android.okalman.kiwi_app.calbacks.IObjectCallback;
import com.android.okalman.kiwi_app.db.DbFlightManager;
import com.android.okalman.kiwi_app.db.objects.Flight;
import com.android.okalman.kiwi_app.json.FlightHttpJsonParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by okalman on 1.10.17.
 * This asynctask loads data from api, stors them in DB and pass them via callback
 * If app has fresh data, calling API is skipped.
 */

public class AsyncDataGetter extends AsyncTask<Void, String, List<Flight>> {

    private HttpURLConnection connection = null;
    private Context context;
    private IObjectCallback callback;

    public AsyncDataGetter(Context context, IObjectCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected List<Flight> doInBackground(Void... params) {
        try {

            DbFlightManager flightManager = DbFlightManager.getInstance(context);
            List<Flight> flightsFromDb = flightManager.getTodayFlights();
            if (flightsFromDb.size() == 5) {
                return flightsFromDb;
            } else if (flightsFromDb.size() > 0) {
                //this fixes state when there are not enough or too many flights in db
                flightManager.invalidateTodayRecords();
            }
            List<Flight> flightsFromServer = FlightHttpJsonParser.parseFlights(sendGet());
            Collections.shuffle(flightsFromServer); //make things interesting
            flightManager.fillUpDatabase(flightsFromServer);
            return flightManager.getTodayFlights();
        } catch (Exception e) {
            Log.w("Loading data FAILURE", e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Flight> flights) {
        if (callback != null) {
            callback.callbackCall(this.getClass(), flights);
        }
    }


    private String sendGet() throws Exception {
        String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        Date future = Calendar.getInstance().getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(future);
        c.add(Calendar.DATE, 3);
        future = c.getTime();
        String futureDate = new SimpleDateFormat("dd/MM/yyyy").format(future);
        String url = context.getString(R.string.server_url_skypicker_flights);
        String params = "flyFrom=CZ&dateFrom=" + URLEncoder.encode(currentDate, "UTF-8") + "&dateTo=" +
                URLEncoder.encode(futureDate, "UTF-8") + "&partner=picky&partner_market=us" +
                "&maxstopovers=2&limit=30";

        Log.d("My generated URL:", url + params);
        URL obj = new URL(url + params);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header

        int responseCode = con.getResponseCode();
        Log.d("GET in process", "Sending 'GET' request to URL : " + url);
        Log.d("GET in process", "Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        Log.d("GET in process", "Response: " + response.toString());
        return response.toString();

    }

}
