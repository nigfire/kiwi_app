package com.android.okalman.kiwi_app;

import android.content.Context;

import java.io.File;
import java.util.Calendar;

/**
 * Created by okalman on 2.10.17.
 * General handy methods are here
 */

public class Utils {

    public static long getCurrentDayAsLong() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();

    }

    public static boolean fileExist(String fname, Context context) {
        File file = context.getFileStreamPath(fname);
        return file.exists();
    }
}
