package com.android.okalman.kiwi_app.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.android.okalman.kiwi_app.Utils;
import com.android.okalman.kiwi_app.db.objects.Flight;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by okalman on 1.10.17.
 * This manager is used to manipulate data in database
 */

public class DbFlightManager {

    private static DbFlightManager instance;
    private MyDbHelper dbHelper;

    private DbFlightManager(Context context) {
        this.dbHelper = MyDbHelper.getInstance(context);
    }

    public static DbFlightManager getInstance(Context context) {
        if (instance == null) {
            instance = new DbFlightManager(context);
        }
        return instance;
    }

    public synchronized void fillUpDatabase(List<Flight> flights) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        invalidateOld(db);
        insert(db, flights, getAllIdsFromDatabase(db));
        db.close();
    }

    public synchronized List<Flight> getTodayFlights() {
        List<Flight> flights = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String currentDate = String.valueOf(Utils.getCurrentDayAsLong());
        String selection = Flight.COLUMN_DAY_TO_SHOW + " = ? AND " + Flight.COLUMN_INVALIDATED + " = ?";
        String[] selectionArgs = {currentDate, "0"};
        Cursor cursor = db.query(Flight.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        while (cursor.moveToNext()) {
            Flight f = new Flight(cursor);
            Log.d("DB", "query for today flights " + f.getId() + " " + f.getToCity());
            flights.add(f);

        }
        db.close();
        return flights;
    }


    private void insert(SQLiteDatabase db, List<Flight> flights, Set<String> existingFlights) {
        int counter = 0;
        for (Flight flight : flights) {
            if (!existingFlights.contains(flight.getId())) {
                Log.d("DB", "inserting new flight today " + flight.getId() + " " + flight.getToCity());
                flight.setDayToShow(Utils.getCurrentDayAsLong());
                db.insert(Flight.TABLE_NAME, null, flight.getContentValues());
                counter++;
            } else {
                Log.d("INSERT SKIPPED", "Found invalidted id" + flight.getId());
            }
            if (counter == 5) {
                break;
            }
        }
    }

    public synchronized void invalidateTodayRecords() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Flight.COLUMN_INVALIDATED, 1);
        String currentDate = String.valueOf(Utils.getCurrentDayAsLong());
        Log.d("DB", "Invalidating today records, today date is" + currentDate);
        String selection = Flight.COLUMN_INVALIDATED + " = ? AND " + Flight.COLUMN_DAY_TO_SHOW + " = ?";
        String[] selectionArgs = {"0", currentDate};
        db.update(Flight.TABLE_NAME, values, selection, selectionArgs);
        db.close();
    }

    private void invalidateOld(SQLiteDatabase db) {

        ContentValues values = new ContentValues();
        values.put(Flight.COLUMN_INVALIDATED, 1);
        String currentDate = String.valueOf(Utils.getCurrentDayAsLong());
        Log.d("DB", "Invalidating old records, today date is" + currentDate);
        String selection = Flight.COLUMN_INVALIDATED + " = ? AND " + Flight.COLUMN_DAY_TO_SHOW + " < ?";
        String[] selectionArgs = {"0", currentDate};
        db.update(Flight.TABLE_NAME, values, selection, selectionArgs);
    }

    private Set<String> getAllIdsFromDatabase(SQLiteDatabase db) {
        Set<String> ids = new HashSet<>();

        String[] columns = {Flight.COLUMN_ID};
        Cursor cursor = db.query(Flight.TABLE_NAME, columns, null, null, null, null, null);
        while (cursor.moveToNext()) {
            Log.d("DB", "Get all ids " + cursor.getString(0));
            ids.add(cursor.getString(0));
        }
        return ids;
    }


}
