package com.android.okalman.kiwi_app;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.android.okalman.kiwi_app.adapters.FlightPagerAdapter;
import com.android.okalman.kiwi_app.async.fregmentHolders.TaskFragmentDataGetter;
import com.android.okalman.kiwi_app.async.fregmentHolders.TaskFragmentImageDownloader;
import com.android.okalman.kiwi_app.calbacks.ICallback;
import com.android.okalman.kiwi_app.calbacks.IObjectCallback;
import com.android.okalman.kiwi_app.db.objects.Flight;

import java.util.List;

/**
 * Main an only activity in this project
 */
public class MainActivity extends AppCompatActivity implements ICallback, IObjectCallback {
    private static final String TAG_IMAGE_DOWNLOADER = "TAG_IMAGE_DOWNLOADER";
    private static final String TAG_DATA_GETTER = "TAG_DATA_GETTER";
    private TaskFragmentImageDownloader imageDownloaderFragment;
    private TaskFragmentDataGetter dataGetterFragment;

    private ContentLoadingProgressBar progressBar;
    private Toolbar toolbar;
    private ViewPager pager;
    private List<Flight> flights;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupViews();
        toolbar.setTitle(R.string.title);
        progressBar.show();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        FragmentManager fm = getSupportFragmentManager();
        dataGetterFragment = (TaskFragmentDataGetter) fm.findFragmentByTag(TAG_DATA_GETTER);
        if (dataGetterFragment == null) {
            dataGetterFragment = new TaskFragmentDataGetter();
            fm.beginTransaction().add(dataGetterFragment, TAG_DATA_GETTER).commit();
        }

    }

    private void setupViews() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        pager = (ViewPager) findViewById(R.id.pager);
        progressBar = (ContentLoadingProgressBar) findViewById(R.id.progressBar);

    }

    @Override
    public void callbackCall(Class callerInstance) {
        progressBar.hide();
        pager.setAdapter(new FlightPagerAdapter(getSupportFragmentManager(), flights));
    }

    @Override
    public void callbackCall(Class caller, Object data) {
        if (caller.equals(TaskFragmentDataGetter.class)) {
            if (data != null) {
                List<Flight> flights = (List<Flight>) data;
                FragmentManager fm = getSupportFragmentManager();
                this.flights = flights;
                imageDownloaderFragment = (TaskFragmentImageDownloader) fm.findFragmentByTag(TAG_IMAGE_DOWNLOADER);

                //we need to obtain images when we have flights
                if (imageDownloaderFragment == null) {
                    imageDownloaderFragment = TaskFragmentImageDownloader.getInstance(flights);
                    fm.beginTransaction().add(imageDownloaderFragment, TAG_IMAGE_DOWNLOADER).commitAllowingStateLoss();
                }
            } else {
                connectionErrorDialog();
            }

        }

    }

    private void connectionErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton(R.string.connection_failed_retry, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dataGetterFragment.forceTaskRerun();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.connection_failed_close_app, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MainActivity.this.finish();
            }
        });
        builder.setTitle(R.string.connection_failed_title);
        builder.setMessage(R.string.connection_failed_message)
                .show();
    }
}
